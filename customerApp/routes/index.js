'use strict'
// npm modules setup
var 
    request      = require('request-promise'),
    router = require('express').Router();

// api endpoints starts here---------------------------------------------------------------------
router.get('/', (req, res) => {
   res.render('index');
});

// module exports
module.exports = router;
