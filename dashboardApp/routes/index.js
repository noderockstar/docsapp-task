'use strict'
// npm modules setup
var 
    request      = require('request-promise'),
    router = require('express').Router();

// api endpoints starts here---------------------------------------------------------------------
router.get('/', (req, res) => {
    let url = 'http://localhost:8080/dashboard'
    request.get(url)
        .then((result) => {
            res.render('index', { title: 'Dashboard' , result: result });
        })
        .catch((err) => { console.error(err); res.status(500).json(err); })
});

// module exports
module.exports = router;
