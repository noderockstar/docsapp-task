'use strict'

// node modules set up ====================================================
var express = require('express'),
    app = express(),
    helmet = require('helmet'),
    path = require('path'),
    bodyParser = require('body-parser');


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// Express Middleware settings
app.use(helmet.hidePoweredBy());
app.use(bodyParser.urlencoded({ 'extended': 'true' }));
app.use(bodyParser.json());

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals
    res.locals.message = err.message;
    res.locals.error = err;
    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

// routes ==================================================================

app.use('/', require('./routes'));

// listen (start app with node server.js) ===================================
app.listen(4000);
console.log("App listening on port " + 4000);
