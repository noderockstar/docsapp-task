'use strict'

module.exports = function (sequelize, DataTypes) {
    var Customer = sequelize.define('Customer', {
        name: {
            type: DataTypes.STRING(50),
            allowNull: true
        }
    },
        {
            classMethods: {
                associate: function (models) {
                    Customer.hasMany(models.Request);
                }
            }
        });
    return Customer;
}