
'use strict'

module.exports = function (sequelize, DataTypes) {
    var Request = sequelize.define('Request', {
        pickup_time: {
            type: DataTypes.STRING(50),
            allowNull: true
        },
        status: {
            type: DataTypes.STRING(50),
            defaultValue: 'waiting',
            allowNull: true
        }
    },
        {
            classMethods: {
                associate: function (models) {
                    Request.belongsTo(models.Driver);
                    Request.belongsTo(models.Customer);
                }
            }
        });
    return Request;
}