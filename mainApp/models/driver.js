'use strict'

module.exports = function (sequelize, DataTypes) {
    var Driver = sequelize.define('Driver', {
        name: {
            type: DataTypes.STRING(50),
            allowNull: true
        }
    },
        {
            classMethods: {
                associate: function (models) {
                    Driver.hasMany(models.Request);
                }
            }
        });
    return Driver;
}
