
'use strict';

var fs = require('fs');
var path = require('path');
var Sequelize = require('sequelize');
var config = require('../config');

var sequelize = new Sequelize(config.DB_SCHEMA,
                              config.DB_USERNAME,
                              config.DB_PASSWORD,
                              { host: config.DB_HOST, 
                                port: config.DB_PORT,
                                dialect: 'mysql', 
                                logging: false,
                                pool: {
                                  max: 5,
                                  min: 0,
                                  idle: 10000
                              }
                              });

var db = {};

sequelize.sync({force:true},function (err) {
  if(err){
     console.log('An error occur while creating table');
  }else{
     console.log('Item table created successfully');
  }
 });

fs
  .readdirSync(__dirname)
  .filter(function(file) {
    return (file.indexOf('.') !== 0) && (file !== 'index.js');
  })
  .forEach(function(file) {
    var model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(function(modelName) {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
