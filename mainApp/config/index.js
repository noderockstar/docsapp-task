
module.exports = { 
    DB_USERNAME : 'root',
    DB_PASSWORD : 'root',
    DB_HOST : 'localhost',
    DB_SCHEMA: 'docsapp',
    DB_PORT: 3306,
    port : process.env.PORT || 8080
};