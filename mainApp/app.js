'use strict'

// node modules set up ====================================================
 var express = require('express'),
     app = express(),												
     config = require('./config'),			
     helmet = require('helmet'),
     bodyParser = require('body-parser'),
     jwt    = require('jsonwebtoken');

// Express Middleware settings
app.use(helmet.hidePoweredBy());
app.use(bodyParser.urlencoded({'extended': 'true'})); 
app.use(bodyParser.json());



// routes ==================================================================

app.use('/', require('./routes'));

// listen (start app with node server.js) ===================================
app.listen(config.port);
console.log("App listening on port " + config.port);
