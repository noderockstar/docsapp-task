'use strict'
// npm modules setup
var 
    config = require('../config'),
    models = require('../models'),
    _      = require('underscore'),
    moment = require('moment'),
    router = require('express').Router();

// api endpoints starts here---------------------------------------------------------------------

//------------------ driver apis section-------------------
router.post('/driver', (req, res) => {
    models.Request.findAll({
        where: { DriverId: req.body.driverId }
    })
        .then((data) => {
            let result = _.groupBy(data, (obj) => { return obj.status });
            res.status(200).json(result);
        })
        .catch((err) => { console.error(err); res.status(500).json(err); })
});

router.post('/driver/pickup', (req, res) => {
    models.Request.findOne({
        where: { id: req.body.requestId }
    })
        .then((data) => {
            if(data.status === 'waiting') {
               data.update({
                   status: "ongoing",
                   DriverId: req.body.driverId
               })
               .then((updated) => {
                   setTimeout(reqCompletion(data),  300000);
                   res.status(200).json({ message: "success"});
               })
            } else{
                res.status(200).json({ message: "Request no longer available" });
            }
        })
        .catch((err) => { console.error(err); res.status(500).json(err); })
});

//------------------ dashboard apis section-------------------
router.get('/dashboard', (req, res) => {
    console.log("hi")
    models.Request.findAll({})
        .then((data) => {
            console.log(data);
            res.status(200).json(data);
        })
        .catch((err) => { console.error(err); res.status(500).json(err); })
});




//------------------ customer apis section-------------------
// customer makes this request
router.post('/request/add', (req, res) => {
    return models.Request.create({
        CustomerId: req.body.customerId
    })
        .then((data) => {
            // active drivers
            models.Request.findAll({
                attributes: ['id'],
                include: [{
                    model: models.Driver,
                    attributes: ['id', 'status'],
                    // where: { status: {  $ne: 'ongoing'} }
                }]
            })
            .then((drivers) => {
                let activeDriverIds = _.pluck(drivers, 'id');
                // socket pushes notification to this drivers 
            })
            res.status(200).json(data);
        })
        .catch((err) => { console.error(err); res.status(500).json(err); })
});




function reqCompletion(data) {
   return data.update({ status: "complete"})
}



// module exports
module.exports = router;
