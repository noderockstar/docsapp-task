# NodeJS developer task of Instarem

Node REST APIs using Mongoose, Express and Jsonwebtokens.

## Requirements

- [Node and npm](http://nodejs.org)

## Installation

1. Clone the repository: `git clone https://github.com/ashokramadasu/instarem-task`
2. Install the application: `npm install`
3. Start the server: `node app.js` or `npm start`

