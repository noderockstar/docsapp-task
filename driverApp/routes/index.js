'use strict'
// npm modules setup
var 
    request      = require('request-promise'),
    router = require('express').Router();

// api endpoints starts here---------------------------------------------------------------------
router.get('/', (req, res) => {
    let options = {
        method: 'POST',
        uri: 'http://localhost:8080/driver',
        body: {
            driverId: req.query.id
        },
        json: true 
    };

    request.post(options)
        .then((result) => {
            res.render('index', { title: 'Driver' , result: result });
        })
        .catch((err) => { console.error(err); res.status(500).json(err); })
});


router.get('/pickup', (req, res) => {
    let options = {
        method: 'POST',
        uri: 'http://localhost:8080/driver/pickup',
        body: {
            requestId: requestId
        },
        json: true 
    };

    request.post(options)
        .then((result) => {
            res.status(200).json(result);
        })
        .catch((err) => { console.error(err); res.status(500).json(err); })
});

// module exports
module.exports = router;